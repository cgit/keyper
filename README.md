Keyper
------

A very simple program to be used with sshd to transfer keys to authorized
servers. Mostly to be used with the Dracut module `acquire-key-over-ssh` for
machines to acquire their encryption keys securely over ssh from a secure
server. (It technically just is a way to dump a file upon login to an ssh
connection.).

To set this up, run the following on the keyserver, replacing `<id_rsa.pub>`
with the public key of the authorized user:

```bash
$ gcc -o keyper keyper.c
$ sudo su
# useradd keyper
# cp keyper /home/keyper
# cd /home/keyper
# chsh -s /home/keyper/keyper keyper
# mkdir .ssh
# echo 'environment="KEYPER_FILE=/home/keyper/keyper-key" <id_rsa.pub>' >> .ssh/authorized_keys
# chown -R keyper:keyper .ssh/
# chmod 700 .ssh
# head -c 512 /dev/urandom | base64 -w0 > keyper-key
```

Make sure `PermitUserEnvironment` is set to "yes" in sshd\_config.

TL;DR this sets up a user, keyper, sets its shell to "keyper", which reads a
file based on an environement variable. Then it sets up an authorized key and
sets the environment based on the authorized ssh key. Thereby multiple different
keys can be served different authorized keys.

There are some weird things that can happen with a binary key. For example,
carridge returns may be removed, so to avoid these, the above commands
base64-encode the key.

Note that the keyper files must be absolute paths.

On the client, run:

```
$ ssh keyper@keyserver > /tmp/key
$ sudo luksAddKey /dev/<disk> /tmp/key
$ shred /tmp/key
```

## Disabling Keyper

Keyper can be temporarily disabled by `touch`-ing a file. This file defaults to
`keyper-disabled` (in keyper's HOME directory), but can be overriden to any file
by setting the environment `KEYPER_DISABLE_FILE`. When this file exists, keyper
will exit with an error.
