#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

static int check_fname(const char* fname)
{
  while (*fname) {
    if (strchr("/.", *(fname++))) {
      return 0;
    }
  }
  return 1;
}

static int dump_file(const char* fname)
{
  int fd = open(fname, O_RDONLY);
  if (fd == -1) {
    fprintf(stderr, "Failed to open %s\n", fname);
    return 1;
  }
  char buf[1024];
  int buflen;
  while ((buflen = read(fd, buf, sizeof(buf))) > 0) {
    write(1, buf, buflen);
  }
  close(fd);
  return 0;
}

int main(int argc, char** argv)
{
  const char* file = getenv("KEYPER_FILE");
  const char* disable_file_env = getenv("KEYPER_DISABLE_FILE");
  char disable_file[4096];

  if (!disable_file_env) {
    const char* home = getenv("HOME");
    if (!home) {
      fprintf(stderr, "No $HOME environment variable\n");
      return 2;
    }
    snprintf(disable_file, sizeof(disable_file) - 1, "%s/keyper-disable", home);
  } else {
    strncpy(disable_file, disable_file_env, sizeof(disable_file) - 1);
  }

  if (!file) {
    fprintf(stderr, "No KEYPER_FILE value.\n");
    return 1;
  }

  if (disable_file[0] != '/') {
    fprintf(stderr, "$KEYPER_DISABLE_FILE must be an absolute path!\n");
    return 1;
  }

  if (file[0] != '/') {
    fprintf(stderr, "$KEYPER_FILE must be an absolute path!\n");
    return 1;
  }

  struct stat statbuf;
  if (!stat(disable_file, &statbuf)) {
    fprintf(stderr, "Keyper disabled because %s exists.\n", disable_file);
    return 127;
  }

  return dump_file(file);
}
